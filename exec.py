import os
from shutil import copyfile

print("'exit' eingeben zum beenden")
while True:
    title = input("Title Zeile eingeben:\n")
    if title == "exit":
        exit(0)
    subtitle = input("Subtitle Zeile eingeben:\n")
    if subtitle == "exit":
        exit(0)
    land = input("Land eingeben (bitte in Englisch und mit llerzeichen o.ä. bspw.: germany, united kingdom):\n")
    if land == "exit":
        exit(0)



    print(title, ", ", subtitle, ", ", land)

    with open("index.html") as f:
        lines = [line.rstrip() for line in f]

    for i in range(1, len(lines)):
        lines[i] = lines[i].replace("${TITLE}", title)
        lines[i] = lines[i].replace("${SUBTITLE}", subtitle)
    
    with open("output.html", "w") as o:
        o.writelines("%s\n" % i for i in lines)

    land = land.lower()
    land = land.replace(" ", "_")
    
    try:
        os.remove("./img/current.png")
    except IOError as e:
        print("")

    try:
        copyfile("./img/all/" + land + ".png", "./img/current.png")
    except IOError as e:
        print("Hier lief was falsch, hast du das land falsch eingegeben ?\n")
    
